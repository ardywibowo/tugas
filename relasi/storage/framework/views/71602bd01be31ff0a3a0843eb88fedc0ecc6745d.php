<!doctype html>
<html lang="en">
<head>
    <title>List Kota/Kabupaten</title>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/app.css')); ?>">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">List Kota/Kabupaten</h2>
        <div class="row p-3">
            <a href="/desa" class="btn btn-primary col-2 mr-2">Lihat Daftar Desa</a>
            <a href="/kecamatan" class="btn btn-primary col-2 mr-2">Lihat Daftar Kecamatan</a>
            <a href="/kota" class="btn btn-primary col-2 mr-2">Lihat Daftar Kota</a>
            <a href="/provinsi" class="btn btn-primary col-2">Lihat Daftar Provinsi</a>
        </div>
        <a href="/kota/tambah" class="btn btn-primary col-2">Input Kota Baru</a>
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="atasan">Kota/Kabupaten</th>
                <th id="atasan">Provinsi</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($d->id); ?></td>
                    <td><?php echo e($d->nama); ?></td>
                    <td><?php echo e($d->provinsi->nama); ?></td>
                    <td>
                        <a href="/kota/edit/<?php echo e($d->id); ?>" class="btn btn-primary">Edit</a>
                        <a href="/kota/hapus/<?php echo e($d->id); ?>" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</div>
</body>
<?php /**PATH /home/ardy/Documents/tugas/tugas/tugas/relasi/resources/views/tampilkota.blade.php ENDPATH**/ ?>