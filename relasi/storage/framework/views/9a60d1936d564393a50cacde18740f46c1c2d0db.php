<!doctype html>
<html lang="en">
<head>
    <title>Edit Kota/Kabupaten</title>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/app.css')); ?>">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Edit Kota/Kabupaten</h2>
        <form method="post" action="/kota/update">

            <?php echo e(csrf_field()); ?>


            <div class="form-group">
                <input type="hidden" name="id" value="<?php echo e($data[0]->id); ?>">
                <label>Nama</label>
                <input type="text" name="nama" class="form-control" required value="<?php echo e($data[0]->nama); ?>">
            </div>
            <div class="form-group">
                <label>Provinsi</label>
                <input type="number" name="provinsi_id" class="form-control" required value="<?php echo e($data[0]->provinsi_id); ?>">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Simpan">
            </div>

        </form>
    </div>
</div>
</body>
<?php /**PATH /home/ardy/Documents/tugas/tugas/tugas/relasi/resources/views/editkota.blade.php ENDPATH**/ ?>