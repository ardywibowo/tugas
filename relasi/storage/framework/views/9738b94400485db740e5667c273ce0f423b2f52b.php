<!doctype html>
<html lang="en">
<head>
    <title>List Film</title>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/app.css')); ?>">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">List Film</h2>
        <a href="/desa/tambah" class="btn btn-primary col-2">Input Film Baru</a>
    </div>
    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="card p-3">
            <h3 class="card-title mt-2 mb-3"><?php echo e($d->id); ?>. <?php echo e($d->judul); ?></h3>
            <a href="/desa/tambah" class="btn btn-primary col-2">Input Penonton Baru</a>
            <table class="table table-striped mt-3">
                <thead>
                <tr>
                    <th id="id">Id</th>
                    <th id="penonton">Penonton</th>
                    <th id="opsi">Opsi</th>
                </tr>
                </thead>
                <tbody>
                <?php $__currentLoopData = $d->penonton; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($p->id); ?></td>
                        <td><?php echo e($p->nama); ?></td>
                        <td>
                            <a href="/desa/edit/<?php echo e($d->id); ?>" class="btn btn-primary">Edit</a>
                            <a href="/desa/hapus/<?php echo e($d->id); ?>" class="btn btn-danger">Hapus</a>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
</body>
<?php /**PATH /home/ardy/Documents/tugas/tugas/tugas/relasi/resources/views/tampilfilm.blade.php ENDPATH**/ ?>