<!doctype html>
<html lang="en">
<head>
    <title>Tambah Provinsi</title>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/app.css')); ?>">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Tambah Provinsi</h2>
        <form method="post" action="/provinsi/simpan">

            <?php echo e(csrf_field()); ?>


            <div class="form-group">
                <label class="form-label">Nama</label>
                <input type="text" name="nama" class="form-control" required>
            </div>
            <div class="form-group">
                <label class="form-label">Atasan_id</label>
                <input type="number" name="atasan_id" class="form-control" required>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Simpan">
            </div>

        </form>
    </div>
</div>
</body>
<?php /**PATH /home/ardy/Documents/tugas/tugas/tugas/relasi/resources/views/tambahprovinsi.blade.php ENDPATH**/ ?>