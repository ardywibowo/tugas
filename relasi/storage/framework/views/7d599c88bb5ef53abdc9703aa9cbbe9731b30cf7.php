<!doctype html>
<html lang="en">
<head>
    <title>List Daerah</title>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/app.css')); ?>">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">List Employee</h2>
        <a href="/employee/tambah" class="btn btn-primary col-2">Input Pegawai Baru</a>
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="nama">Desa</th>
                <th id="atasan">Kecamatan</th>
                <th id="atasan">Kota/Kabupaten</th>
                <th id="atasan">Provinsi</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($d->id); ?></td>
                    <td><?php echo e($d->nama); ?></td>
                    <td><?php echo e($d->kecamatan->nama); ?></td>
                    <td><?php echo e($d->kecamatan->kota_kabupaten->nama); ?></td>
                    <td><?php echo e($d->kecamatan->kota_kabupaten->provinsi->nama); ?></td>
                    <td>
                        <a href="/employee/edit/<?php echo e($d->id); ?>" class="btn btn-primary">Edit</a>
                        <a href="/employee/hapus/<?php echo e($d->id); ?>" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</div>
</body>
<?php /**PATH /home/ardy/Documents/tugas/tugas/tugas/relasi/resources/views/tampil.blade.php ENDPATH**/ ?>