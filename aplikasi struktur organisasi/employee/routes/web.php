<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\CompanyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/employee',[EmployeeController::class, 'index']);
Route::get('/employee/tambah',[EmployeeController::class, 'tambah']);
Route::post('/employee/simpan',[EmployeeController::class, 'simpan']);
Route::get('/employee/edit/{id}',[EmployeeController::class, 'edit']);
Route::post('/employee/update',[EmployeeController::class, 'update']);
Route::get('/employee/hapus/{id}',[EmployeeController::class, 'hapus']);

//export
Route::get('/employee/export/', [EmployeeController::class, 'export']);
Route::get('/employee/exportpdf/', [EmployeeController::class, 'exportpdf']);

Route::get('/company',[CompanyController::class, 'index']);
Route::get('/company/tambah',[CompanyController::class, 'tambah']);
Route::post('/company/simpan',[CompanyController::class, 'simpan']);
Route::get('/company/edit/{id}',[CompanyController::class, 'edit']);
Route::post('/company/update',[CompanyController::class, 'update']);
Route::get('/company/hapus/{id}',[CompanyController::class, 'hapus']);
