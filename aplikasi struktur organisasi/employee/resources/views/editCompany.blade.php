<!doctype html>
<html lang="en">
<head>
    <title>Edit Company</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Edit Company</h2>
        <form method="post" action="/company/update">

            {{ csrf_field() }}

            <div class="form-group">
                <input type="hidden" name="id" value="{{ $data[0]->id }}">
                <label>Nama Company</label>
                <input type="text" name="nama" class="form-control" required value="{{$data[0]->nama}}">
            </div>
            <div class="form-group">
                <label>Alamat Company</label>
                <input type="text" name="alamat" class="form-control" required value="{{$data[0]->alamat}}">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Simpan">
            </div>

        </form>
    </div>
</div>
</body>
