<table>
    <thead>
    <tr>
        <th>Id</th>
        <th>Nama</th>
        <th>Posisi</th>
        <th>Perusahaan</th>
    </tr>
    </thead>
    <tbody>
    @foreach($employee as $e)
        <tr>
            <td>{{ $e->id }}</td>
            <td>{{ $e->nama }}</td>
            <td>{{ $e->posisi->nama }}</td>
            <td>{{ $e->company->nama }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
