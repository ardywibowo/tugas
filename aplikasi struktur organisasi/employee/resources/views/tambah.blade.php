<!doctype html>
<html lang="en">
<head>
    <title>Tambah Employee</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Tambah Employee</h2>
        <form method="post" action="/employee/simpan">

            {{ csrf_field() }}

            <div class="form-group">
                <label class="form-label">Nama</label>
                <input type="text" name="nama" class="form-control" required>
            </div>
            <div class="form-group">
                <label class="form-label">Atasan</label></br>
                <select class="form-select" aria-label="Default select example" name="atasan_id" required>
                    <option selected value="">Pilih Atasan</option>
                    @foreach($employee as $e)
                        <option value="{{$e->id}}">{{$e->nama}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label class="form-label">Company</label></br>
                <select class="form-select" aria-label="Default select example" name="company_id" required>
                    <option selected value="">Pilih Company</option>
                    @foreach($company as $c)
                    <option value="{{$c->id}}">{{$c->nama}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Simpan">
            </div>

        </form>
    </div>
</div>
</body>
