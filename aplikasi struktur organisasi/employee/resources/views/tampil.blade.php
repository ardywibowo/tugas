<!doctype html>
<html lang="en">
<head>
    <title>List Employee</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">List Employee</h2>
        <div class="row p-3">
            <a href="/employee/tambah" class="btn btn-primary col-2 mr-2">Input Pegawai Baru</a>
            <a href="/employee/exportpdf" class="btn btn-primary col-2 mr-2">Export PDF</a>
            <a href="/employee/export" class="btn btn-primary col-2">Export Excel</a>
        </div>
        <table aria-describedby="list employee" class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="nama">Nama</th>
                <th id="atasan">Atasan_id</th>
                <th id="company">Company_id</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $d)
                <tr>
                    <td>{{ $d->id }}</td>
                    <td>{{ $d->nama }}</td>
                    <td>{{ $d->employee->nama ?? 'tidak ada atasan' }}</td>
                    <td>{{ $d->company->nama }}</td>
                    <td>
                        <a href="/employee/edit/{{ $d->id }}" class="btn btn-primary">Edit</a>
                        <a href="/employee/hapus/{{ $d->id }}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>
