<?php

namespace App\Exports;

use App\Employee;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class Export implements FromView
{
    use Exportable;

    public function view(): View
    {
        return view('export', [
            'employee' => Employee::all()
        ]);
    }

}
