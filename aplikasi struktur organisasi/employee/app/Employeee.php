<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employeee extends Model
{
    protected $table = 'employee';

    public function employee()
    {
        return $this->hasMany('App\Employee');
    }
}
