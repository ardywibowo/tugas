<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employee';

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function employee()
    {
        return $this->belongsTo('App\Employeee','atasan_id');
    }
    public function posisi()
    {
        return $this->belongsTo('App\Posisi');
    }
}
