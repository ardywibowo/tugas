<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Employee;
use App\Company;
use App\Exports\Export;

class EmployeeController extends Controller
{
    public function index()
    {
        $data = Employee::all();
        return view('tampil', ['data'=>$data]);
    }

    public function tambah()
    {
        $employee = Employee::all();
        $company = Company::all();
        return view('tambah', ['employee'=>$employee, 'company'=>$company]);
    }

    public function simpan(Request $request)
    {
        DB::table('employee')->insert([
            'nama' => $request->nama,
            'atasan_id' => $request->atasan_id,
            'company_id' => $request->company_id
        ]);

        return redirect('/employee');
    }

    public function edit($id)
    {
        $data = Employee::find($id);
        $employee = Employee::all();
        $company = Company::all();
        return view('edit', ['data'=>$data, 'employee'=>$employee, 'company'=>$company]);
    }

    public function update(Request $request)
    {
        DB::table('employee')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'atasan_id' => $request->atasan_id,
            'company_id' => $request->company_id
        ]);

        return redirect('/employee');
    }

    public function hapus($id)
    {
        DB::table('employee')->where('id', $id)->delete();

        return redirect('/employee');
    }

    public function export()
    {
        return (new Export)->download('tabel.xlsx');
    }

    public function exportpdf()
    {
        return (new Export)->download('tabel.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
    }
}
