//jumlah complain setiap bulan
SELECT COUNT(*) ,MONTH(DateReceived), YEAR(DateReceived) FROM ConsumerComplaints GROUP BY MONTH(DateReceived), YEAR(DateReceived) ORDER BY YEAR(DateReceived) ASC , MONTH(DateReceived) ASC ;

//complain yang memiliki tag Older American
SELECT * FROM ConsumerComplaints WHERE Tags = 'Older American';
